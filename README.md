# **E-Controller**
**Author: Raymond A. P.**
_Last Updated: 30/5/2020_

### About E-Controller
E-Controller is an Embedded System Project created by Raymond A P.

This app is useful for automate your daily electronic device with microcontroller, moreover you can connect your LINE Account with this app. 

### Requirements
* **E-Controller Server**
    * NodeJS v12.16.0
    * MYSQL 5.7 or newer
* **E-Controller Client**
    * NodeMCU ESP32
    * Network Connection via Wifi
    * [ArduinoJSON v6.15.2](https://github.com/bblanchon/ArduinoJson)
    * [SocketIO Client build 12.10.2018](https://github.com/timum-viw/socket.io-client)
    * [DHT11 Library](https://github.com/adidax/dht11)
    * [AnalogWrite v0.2](https://github.com/ERROPiX/ESP32_AnalogWrite)
    * [Tone32 Library](https://github.com/lbernstone/Tone/)

### Security Vulnerabilities
If you discover a security vulnerability or bug within E-Controller, please send an e-mail to Raymond via [arcphysx@gmail.com](mailto:arcphysx@gmail.com). All security vulnerabilities and bug will be promptly addressed.
