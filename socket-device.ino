#include <ArduinoJson.h>
#include <WiFi.h>
#include <SocketIoClient.h>
#include <analogWrite.h>
#include <dht11.h>
#include <Tone32.h>

#include <vector>

/////////////////////////////////////
////// USER DEFINED VARIABLES //////
///////////////////////////////////
/// WIFI Settings ///
const char* ssid     = "KaliServer2.1";
const char* password = "videorohani123";

/// Socket.IO Settings ///
char host[] = "micro.cium.in"; // Socket.IO Server Address
int port = 443; // Socket.IO Port Address
char path[] = "/backend.io/?authorization=536a4f9cae3940bf81cf145f370ad51b&type=device&transport=websocket"; // Socket.IO Base Path
bool useSSL = true; // Use SSL Authentication
const char * sslFingerprint = "";  // SSL Certificate Fingerprint
bool useAuth = false; // use Socket.IO Authentication
const char * serverUsername = "socketIOUsername";
const char * serverPassword = "socketIOPassword";

/////////////////////////////////////
////// ESP32 Socket.IO Client //////
///////////////////////////////////
SocketIoClient socket;
WiFiClient client;

/////////////////////////////////////
//////   JSON Processor Func  //////
///////////////////////////////////
DynamicJsonDocument jsonTap(2048);

/////////////////////////////////////
//////   Static Object Class  //////
///////////////////////////////////
dht11 DHT11;

/////////////////////////////////////
//////    Custom Variables    //////
///////////////////////////////////
bool webSocketState = false;
const int toogleConnectPin = 13;
bool toogleConnectWait = false;
const int buzzerPin = 14;
int buzzerChannel = analogWriteChannel(buzzerPin);

const String key = "536a4f9cae3940bf81cf145f370ad51b";

unsigned long globalMillis = 0;

/////////////////////////////////////
//////     Boot Music Var     //////
///////////////////////////////////
int melody[] = {
  NOTE_A4, 0, NOTE_A4, NOTE_A4,
  NOTE_C5, 0, NOTE_AS4, NOTE_A4, 
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5,
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5,
  NOTE_AS4, NOTE_AS4, NOTE_AS4, NOTE_AS4,
  NOTE_AS4, NOTE_AS4, NOTE_AS4, NOTE_AS4,
  NOTE_AS4, NOTE_AS4, NOTE_AS4, NOTE_AS4,
  NOTE_AS4, NOTE_AS4, NOTE_AS4, NOTE_AS4,
  NOTE_AS4, NOTE_AS4, NOTE_AS4, NOTE_AS4,
  NOTE_D5, NOTE_D5, NOTE_D5, NOTE_D5,
  NOTE_C5, NOTE_C5, NOTE_C5, NOTE_C5, 
  NOTE_F5, NOTE_F5, NOTE_F5, NOTE_F5, 
  NOTE_G5, NOTE_G5, NOTE_G5, NOTE_G5,
  NOTE_G5, NOTE_G5, NOTE_G5, NOTE_G5, 
  NOTE_G5, NOTE_G5, NOTE_G5, NOTE_G5, 
  NOTE_C5, NOTE_AS4, NOTE_A4, NOTE_F4,
  NOTE_G4, 0, NOTE_G4, NOTE_D5,
  NOTE_C5, 0, NOTE_AS4, 0,
  NOTE_A4, 0, NOTE_A4, NOTE_A4,
  NOTE_C5, 0, NOTE_AS4, NOTE_A4, 
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5,
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5,
  NOTE_G4, 0, NOTE_G4, NOTE_D5,
  NOTE_C5, 0, NOTE_AS4, 0,
  NOTE_A4, 0, NOTE_A4, NOTE_A4,
  NOTE_C5, 0, NOTE_AS4, NOTE_A4, 
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5,
  NOTE_G4,0, NOTE_G4, NOTE_AS5,
  NOTE_A5, NOTE_AS5, NOTE_A5, NOTE_AS5
 };

/////////////////////////////////////
//////  Common Used Request   //////
///////////////////////////////////

// This void will request to server to get all component state
void request_GetAllComponent(){
  jsonTap.clear();
  String result = "";
  jsonTap["key"] = key;
  serializeJson(jsonTap, result);
  char char_array[result.length() + 1]; 
  strcpy(char_array, result.c_str()); 
  socket.emit("request_component_update_all", char_array);
}

// This void will notify server about component state change
void request_BroadcastComponentChanges(String jsonText){
  char char_array[jsonText.length() + 1];
  strcpy(char_array, jsonText.c_str());
  socket.emit("request_component_changes", char_array);
}

/////////////////////////////////////
//////  Custom Helper Class   //////
///////////////////////////////////

// This class will hold component value
class Component{

  public:
    enum TYPE { NOTHING, LED, LED_RGB, SENSOR_TEMPERATURE };

  private:
    std::vector<int> pin;
    TYPE type = TYPE::NOTHING;
    int id = 0;
    bool loop = false;
    int broadcastInterval = 100;
    unsigned long lastBroadcast = 0;

    // LED_RGB propoerty
    int led_rgb_color[3] = { 0, 0, 0};

    // LED propoerty
    int led_state = 0;
    
    // SENSOR_TEMPERATURE propoerty
    float sensor_temperature_humidity = 0.0f;
    float sensor_temperature_temperature = 0.0f;

    void broadcastChanges(){
      if(loop && (globalMillis - lastBroadcast) >= broadcastInterval){

        jsonTap.clear();
        String result = "";
        bool matchAnyType = false;

        switch(type){
          case TYPE::SENSOR_TEMPERATURE:
            matchAnyType = true;
            jsonTap["id"] = id;
            jsonTap["humidity"] = sensor_temperature_humidity;
            jsonTap["temperature"] = sensor_temperature_temperature;
            break;
        }

        if(matchAnyType){
          serializeJson(jsonTap, result);
          request_BroadcastComponentChanges(result);
        }

        lastBroadcast = globalMillis;
      }
    }

  public:
    int getId(){ return id; }
    bool getLoop(){ return loop; }

    void setComponentAs(TYPE t, JsonVariant jsonVariant){
      id = jsonVariant["id"];
      
      pin.clear();
      for (JsonVariant p : jsonVariant["value"]["pin"].as<JsonArray>()) {
        pin.push_back(p);
      }

      type = t;
      switch(type){
        case TYPE::LED:
          led_state = jsonVariant["value"]["state"];
          break;
        
        case TYPE::LED_RGB:
          try{
            led_rgb_color[0] = jsonVariant["value"]["color"][0];
            led_rgb_color[1] = jsonVariant["value"]["color"][1];
            led_rgb_color[2] = jsonVariant["value"]["color"][2];
          }catch(std::exception e){
            led_rgb_color[0] = 0;
            led_rgb_color[1] = 0;
            led_rgb_color[2] = 0;
          }
          break;

        case TYPE::SENSOR_TEMPERATURE:
          loop = true;
          sensor_temperature_humidity = 0.0f;
          sensor_temperature_temperature = 0.0f;
          try{
            broadcastInterval = jsonVariant["value"]["interval"];
          }catch(std::exception e){
            broadcastInterval = 100;
          }
          break;
      }
    };

    void refreshComponent(){
      if(pin.size() <= 0) return;
      switch(type){
        case TYPE::LED:
          pinMode(pin.at(0), OUTPUT);
          digitalWrite(pin.at(0), (led_state % 2));
          break;
        
        case TYPE::LED_RGB:
          if(pin.size() < 3 || pin.size() > 3) return;
          for (int i = 0; i < pin.size(); i++){
            int v = 255 - (led_rgb_color[i] % 256);
            pinMode(pin.at(i), OUTPUT);
            analogWrite(pin.at(i), v);
          }
          break;

        case TYPE::SENSOR_TEMPERATURE:
          int chk = DHT11.read(pin.at(0));
          sensor_temperature_humidity = (float)DHT11.humidity;
          sensor_temperature_temperature = (float)DHT11.temperature;
          broadcastChanges();
          break;
      }
    };
};

/////////////////////////////////////
//////  Component Collection  //////
///////////////////////////////////

// This vector will remember all component state
std::vector<Component> componentList;


/////////////////////////////////////
//////  Common Used Function  //////
///////////////////////////////////

// This void will refresh all connected component state
void internal_RefreshComponent(){
  for (Component component : componentList){
    component.refreshComponent();
  }
}

// This void will play the buzzer test sound
void internal_BuzzerBootSound(){
  for (int i = 0; i < 112; i++) {
    int noteDuration = 350 / 4;
    tone(buzzerPin, melody[i], noteDuration, buzzerChannel);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(buzzerPin, buzzerChannel);
  }
}

// This void will play sound while the wifi is connecting...
void internal_BuzzerWifiConnecting(){
  tone(buzzerPin, NOTE_B4, 250, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
}

// This void will play sound on wifi connected
void internal_BuzzerWifiConnected(){
  tone(buzzerPin, NOTE_E7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
  delay(100);
  tone(buzzerPin, NOTE_E7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
}

// This void will play sound on socket connected
void internal_BuzzerSocketConnected(){
  tone(buzzerPin, NOTE_E7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
  tone(buzzerPin, NOTE_F7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
  tone(buzzerPin, NOTE_G7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
}

// This void will play sound on socket disconnected
void internal_BuzzerSocketDisconnected(){
  tone(buzzerPin, NOTE_G7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
  tone(buzzerPin, NOTE_F7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
  tone(buzzerPin, NOTE_E7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
}

// This void will play sound on push button clicked
void internal_BuzzerClickSound(){
  tone(buzzerPin, NOTE_G7, 100, buzzerChannel);
  noTone(buzzerPin, buzzerChannel);
}

/////////////////////////////////////
//////  Handle Socket Event   //////
///////////////////////////////////

// This void will handle once socket connection is established
void socket_Connected(const char * payload, size_t length) {
//  socket.emit("info", "{\"info\":\"This is from NodeMCU32S\"}");
  request_GetAllComponent();
  Serial.println("Socket.IO Connected!");
  internal_BuzzerSocketConnected();
}

// This void will handle data for all component state
void socket_ComponentUpdateAll(const char * payload, size_t length) {
  Serial.println(payload);
  jsonTap.clear();
  DeserializationError error = deserializeJson(jsonTap, payload);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    // request_GetAllComponent();
    return;
  }

  componentList.clear();
  for (JsonVariant component : jsonTap.as<JsonArray>()) {
    Component c;
    if(component["type"].as<String>() == "led"){
      c.setComponentAs(Component::TYPE::LED, component);
      componentList.push_back(c);
      // Serial.println("We have led");
    }else if(component["type"].as<String>() == "led_rgb"){
      c.setComponentAs(Component::TYPE::LED_RGB, component);
      componentList.push_back(c);
      // Serial.println("We have led_rgb");
    }else if(component["type"].as<String>() == "sensor_temperature"){
      c.setComponentAs(Component::TYPE::SENSOR_TEMPERATURE, component);
      componentList.push_back(c);
      // Serial.println("We have sensor_temperature");
    }
  }
  // Serial.println("Refreshing...");
  internal_RefreshComponent();
  // Serial.println("Component State Refreshed");
}

// This void will handle on one component update
void socket_ComponentUpdateOne(const char * payload, size_t length){
  Serial.println(payload);
  jsonTap.clear();
  DeserializationError error = deserializeJson(jsonTap, payload);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    // request_GetAllComponent();
    return;
  }
  
  JsonVariant c = jsonTap.as<JsonObject>();

  for (Component &component : componentList){
    if(component.getId() == c["id"]){
      if(c["type"].as<String>() == "led"){
        component.setComponentAs(Component::TYPE::LED, c);
        // Serial.println("We have led 2");
      }else if(c["type"].as<String>() == "led_rgb"){
        component.setComponentAs(Component::TYPE::LED_RGB, c);
        // Serial.println("We have led_rgb 2");
      }else if(c["type"].as<String>() == "sensor_temperature"){
        component.setComponentAs(Component::TYPE::SENSOR_TEMPERATURE, c);
        // Serial.println("We have sensor_temperature 2");
      }
      // Serial.println("Refreshing...2");
      component.refreshComponent();
      // Serial.println("Component State Refreshed 2");
      break;
    }
  }
  
}

void setup() {
  // Starting Serial
  Serial.begin(115200);

  // Init Socket Toggle Button
  pinMode(toogleConnectPin, INPUT_PULLDOWN);

  // Init Buzzer Pin
  pinMode(buzzerPin, OUTPUT);

  // Test buzzer sound with Music
  internal_BuzzerBootSound();

  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    internal_BuzzerWifiConnecting();
    delay(500);
    Serial.print(".");
  }

  // Play sound on Buzzer once Wifi Connected
  internal_BuzzerWifiConnected();
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Setup 'on' listen events
  socket.on("connect", socket_Connected);
  socket.on("component_update_all", socket_ComponentUpdateAll);
  socket.on("component_update_one", socket_ComponentUpdateOne);

  // Initiate socket address
  if (useSSL) {
    socket.beginSSL(host, port, path, sslFingerprint);
  } else {
    socket.begin(host, port, path);
  }

}

void loop() {
  // Update millis each loop
  globalMillis = millis();

  // Websocket loop
  if(webSocketState){
    socket.loop();

    // Refresh Component that needs loop
    for (Component &component : componentList){
      if(component.getLoop()) component.refreshComponent();
    }
  }

  // Websocket State Toogle
  if(digitalRead(toogleConnectPin) == HIGH && toogleConnectWait == false){
    toogleConnectWait = true;
  }else if(digitalRead(toogleConnectPin) == LOW && toogleConnectWait == true){
    internal_BuzzerClickSound();
    toogleConnectWait = false;

    // If Websocket is enabled, then disable...
    if(webSocketState){
      webSocketState = false;
      socket.disconnect();
      internal_BuzzerSocketDisconnected();
      Serial.println("Disabling Websocket...");

    // If Websocket is disabled, then enable...
    }else{
      webSocketState = true;
      Serial.println("Enabling Websocket...");
    }
  }

}
